import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useStore } from './../context/StoreContext';

export default function Customer() {
  const [burger, setBurger] = useState(null);
  const [userName, setUserName] = useState('');
  const [ingridents, setIngredients] = useState([]);

  const { products, placeOrder } = useStore();

  const addToCart = burgerIndex => {
    setBurger(burgerIndex);
  };

  const addIngredent = selectedIngredient => {
    setIngredients(ingridents => [...ingridents, selectedIngredient]);
  };

  const removeIngredent = selectedIngredient => {
    setIngredients(ingridents => {
      let newCopy = [];
      let foundAny = false;

      ingridents.forEach(ingrident => {
        if (! foundAny && ingrident === selectedIngredient) {
          foundAny = true;
        } else {
          newCopy.push(ingrident);
        }
      });

      if (! foundAny) {
        alert('Did not find.');
      }

      return newCopy;
    });
  };

  const getIngredientCount = selectedIngredient => {
    let count = 0;

    ingridents.forEach(ingrident => {
      if (ingrident === selectedIngredient) {
        count++;
      }
    });

    return count;
  };

  const checkout = () => {
    // TODO : this won't be the case but just adding checks
    if (burger === null && ! ingridents.length && ! userName) {
      return alert('Cart items & name fields are required.');
    }

    placeOrder({
      burger,
      ingridents,
      name: userName,
    })
      .then(() => {
        setBurger(null);
        setIngredients([]);
      }).catch(console.error);
  };

	return (
    <div className="flex justify-center w-full min-h-screen pt-10">
      <div className="flex flex-col w-3/4">
        <div className="flex justify-between items-center">
          <h3 className="text-3xl text-gray-800 font-bold font-sans mt-2">
            Burgers
          </h3>

          <Link className="text-blue-500 underline" to="/admin">
            Admin View
          </Link>
        </div>

        {/* TODO : remove the duplication later */}
        {burger !== null ? (
          <div className="">
            <span className="text-green-500">Choose you favourite ingridents for your burger</span>

            <ul className="flex flex-col gap-4 mt-6">
              {products.ingridents.map((ingrident, index) => (
                <li className="flex justify-between items-center text-lg font-semibold py-2 px-6 rounded-md shadow border border-gray-100 w-96 bg-white" key={index}>
                  <div className="flex flex-col">
                    <span className="">{ingrident.name}</span>

                    <span className="text-sm text-gray-600">${ingrident.price}</span>
                  </div>

                  <div className="flex items-center">
                    <span className="text-sm text-gray-500 mr-2">
                      ({getIngredientCount(index)})
                    </span>

                    <button
                      className="text-sm text-gray-800 font-medium bg-gray-100 px-2 py-2 rounded-full hover:bg-gray-200 focus:outline-none"
                      onClick={() => addIngredent(index)}
                    >
                      <svg className="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6v6m0 0v6m0-6h6m-6 0H6" /></svg>
                    </button>

                    {getIngredientCount(index) ? (
                      <button
                        className="text-sm text-gray-800 font-medium bg-gray-100 px-2 py-2 rounded-full hover:bg-gray-200 focus:outline-none ml-2"
                        onClick={() => removeIngredent(index)}
                      >
                        <svg className="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M20 12H4" /></svg>
                      </button>
                    ) : null}
                  </div>
                </li>
              ))}
            </ul>
          </div>
        ) : (
          <ul className="flex flex-col gap-4 mt-6">
            {products.burgers.map((burger, index) => (
              <li className="flex justify-between items-center text-lg font-semibold py-2 px-6 rounded-md shadow border border-gray-100 w-96 bg-white" key={index}>
                <div className="flex flex-col">
                  <span className="">{burger.name}</span>

                  <span className="text-sm text-gray-600">${burger.price}</span>
                </div>

                <button
                  className="text-sm text-gray-800 font-medium bg-gray-100 px-4 py-2 rounded-md transition hover:bg-gray-200"
                  onClick={() => addToCart(index)}
                >
                  ADD TO CART
                </button>
              </li>
            ))}
          </ul>
        )}

        {burger !== null && ingridents.length ? (
          <div className="mt-6 text-right">
            <div className="">
              <label htmlFor="name" className="font-medium text-gray-700 cursor-pointer mr-2">
                Your name

                <span className="text-red-500">*</span>
              </label>

              <input
                id="name"
                type="text"
                onChange={e => setUserName(e.target.value)}
                defaultValue={userName}
                className="border-2 border-gray-100 bg-white py-1.5 px-2 rounded transition focus:border-gray-300 focus:outline-none"
              />
            </div>

            <button
              className="mt-4 py-2 px-6 bg-indigo-100 rounded transition hover:bg-indigo-200 disabled:bg-indigo-50 disabled:text-gray-400 disabled:cursor-not-allowed"
              disabled={! userName}
              onClick={checkout}
            >
              Checkout
            </button>
          </div>
        ) : null}
      </div>
    </div>
	);
}
