import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { useStore } from './../context/StoreContext';

export default function Admin() {
  const [search, setSearch] = useState('');

  const { orders } = useStore();

  const filteredOrders = orders.filter(({ name }) => name.toLowerCase() === search || name.toLowerCase().includes(search));

  return (
    <div className="flex justify-center w-full min-h-screen pt-10">
      <div className="flex flex-col w-3/4">
        <div className="flex justify-between items-center">
          <h3 className="text-3xl text-gray-800 font-bold font-sans mt-2">
            Orders
          </h3>

          <Link className="text-blue-500 underline" to="/">
            Users View
          </Link>
        </div>

        <div className="mt-4">
          <div className="flex flex-col">
            <label htmlFor="name" className="font-medium text-gray-700 cursor-pointer mr-2">
              Search by customer's name

              <span className="text-red-500">*</span>
            </label>

            <input
              id="name"
              type="text"
              onChange={e => setSearch(e.target.value.toLowerCase().trim())}
              defaultValue={search}
              className="border-2 border-gray-100 bg-white mt-2 py-1.5 px-2 rounded transition focus:border-gray-300 focus:outline-none"
            />
          </div>

          {filteredOrders.length ? (
            <div>
              <ul className="flex flex-col gap-4 mt-6">
                {filteredOrders.map((order, i) => (
                  <li className="flex flex-col text-lg font-semibold py-2.5 px-6 rounded-md shadow border border-gray-100 w-96 bg-white" key={i}>
                    <div className="flex w-full justify-between items-center">
                      <span className="">{order.burger.name}</span>
                      <span className="text-lg font-medium text-gray-600">${order.orderTotal}</span>
                    </div>

                    <ul className="my-2">
                      {order.ingridents.map(({ name, price, quantity }, idx) => (
                        <li className="flex justify-between items-center py-2 px-4 mb-2 bg-gray-50 rounded-md" key={idx}>
                          <h3 className="text-sm font-medium">
                            {name}
                          </h3>

                          <span className="font-medium">
                            ${price}
                            <span className="text-gray-500 ml-1">x {quantity}</span>
                          </span>
                        </li>
                      ))}
                    </ul>
                  </li>
                ))}
              </ul>
            </div>
          ) : 'We haven\'t received any order yet.'}
        </div>
      </div>
    </div>
  );
}
