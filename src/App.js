import React, { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { StoreProvider } from './context/StoreContext';

const AdminPage = lazy(() => import('./pages/Admin'));
const CustomerPage = lazy(() => import('./pages/Customer'));

function App() {
  return (
    <div className="bg-gray-50">
      <StoreProvider>
        <Router>
          <Suspense fallback={<div className="w-full h-screen flex items-center justify-center">Loading...</div>}>
            <Switch>
              <Route component={AdminPage} path="/admin" />
              <Route component={CustomerPage} />
            </Switch>
          </Suspense>
        </Router>
      </StoreProvider>
    </div>
  );
}

export default App;
