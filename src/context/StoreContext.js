import React, { useReducer, useContext, createContext, useMemo } from 'react';

const StoreContext = createContext(null);

export function StoreProvider({ children }) {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'PLACE_ORDER':
        return {
          ...state,
          orders: [
            ...state.orders,
            action.payload,
          ],
        };

      default:
        return;
    }
  }, {
    orders: [],
  });

  const products = useMemo(() => ({
    burgers: [
      {
        id: 1,
        name: 'Burger A',
        price: 50,
      },
      {
        id: 2,
        name: 'Burger B',
        price: 70,
      },
      {
        id: 3,
        name: 'Burger C',
        price: 60,
      },
    ],
    ingridents: [
      {
        id: 1,
        name: 'Cheese',
        price: 10,
      },
      {
        id: 2,
        name: 'Cebbage',
        price: 20,
      },
      {
        id: 3,
        name: 'Tettuce',
        price: 5,
      },
      {
        id: 4,
        name: 'Onion',
        price: 15,
      },
    ],
  }), []);

  const actions = useMemo(() => ({
    placeOrder: async ({ burger, ingridents, name }) => {
      const burgerDetails = products.burgers[burger];
      let orderTotal = burgerDetails.price;
      const ingridentsAggregated = {};

      for (let i = ingridents.length - 1; i >= 0; i--) {
        const ingrident = ingridents[i];

        if (! (ingrident in ingridentsAggregated)) {
          ingridentsAggregated[ingrident] = { ...products.ingridents[ingrident], quantity: 0 };
        }

        orderTotal += ingridentsAggregated[ingrident].price;
        ingridentsAggregated[ingrident].quantity = ingridentsAggregated[ingrident].quantity + 1;
      }

      dispatch({
        type: 'PLACE_ORDER',
        payload: {
          name,
          burger: burgerDetails,
          ingridents: Object.values(ingridentsAggregated),
          orderTotal,
        }
      });
    },
  }), [products]);

  return (
    <StoreContext.Provider value={{ ...state, ...actions, products: products }}>
      {children}
    </StoreContext.Provider>
  );
}

export function useStore() {
  const context = useContext(StoreContext);

  if (context === null) {
    throw new Error('useStore will only be available inside StoreProvider.');
  }

  return context;
}
